Klient
- meno
- žáner
- profesia
- motivácia
- stavy
	+ zranenie
	+ stres
	+ únava
	+ bezvedomie
- inventár
- poznámky
- kód

# DNGN API

## General responses

### Response 401
Occurs when the request is sent without Authorization header.

```
{
	"status": "error",
	"toast": "Could not authorize the user."
}
```

### Response 404
Occurs when unknown path is given to the request and cannot be resolved.

```
{
	"status": "error",
	"toast": "Unknown path."
}
```


## User
User has automatically created access key after first visit to the website. After that the key is stored in the local storage. The key can be changed in the menu.

### Create new user `POST /user`
This request does not require any request body.

#### Response 200
The server returns status and newly generated access key. The key is stored in the database.

```
{
	"status": "success",
	"key": "aaaa-bbbb-cccc-dddd",
	"language": "sk"
}
```

#### Response 500
Happens when server fails to register new user.

```
{
	"status": "error",
	"toast": "Could not register the user. Try again later, please."
}
```

### Get user data `GET /user`
Attempts to return all the user data:
- current theme
- current language preset
- list of the characters

**Headers:**
- Authorization: {access-key}
- Content-Type: "text/json"

#### Response 200
The user based on their access-key was found, data is returned.

```
{
	"status": "success",
	"toast": "User data loaded successfully.",
	"user": {
		"language": "sk",
		"theme": "light"
	},
	"characters": [
		...
	]
}
```

#### Response 404
When the Authorization code is invalid and not found in the database, no data is returned.

```
{
	"status": "warning",
	"toast": "Could not find the user."
}
```


### Save changed settings `PUT /user`
Serves for setting up the theme or language preferences.

**Headers:**
- Authorization: {access-key}
- Content-Type: "text/json"

```
{
	"language": "sk"|"en"|"cs",
	"theme": "light"|"dark"
}
```

#### Response 200
Everything was saved successfuly.

```
{
	"status": "success",
	"toast": "Settings were successfully saved."
}
```

#### Response 400
Happens when malformed data is sent to the server.

```
{
	"status": "warning",
	"toast": "Could not parse given settings. Did you send correctly formed data?"
}
```

#### Response 500
Happens when server fails to save changed data.

```
{
	"status": "error",
	"toast": "Could not update the settings."
}
```


## Character
Contains all the data about single character.

### Create new character `POST /char`
Sent on the **Back** action, when the user gets to the main menu and currently used character does not have set ID.

**Headers:**
- Authorization: {access-key}
- Content-Type: "text/json"

```
{
	"id": null,
	"name": string(32),
	"genre": int,
	"profession": int,
	"motivation": string(1024),
	"states": {
		"wound": bool,
		"stress": bool,
		"exhaustion": bool,
		"unconsciouness": bool
	},
	"inventory": string(2048),
	"notes": string(2048),
	"die": int,
	"code": string(24)
}
```

### Save existing character `PUT /char/{id}`
Stores the changes done to the character.
Invoked on the blur action, or when the **Back** button is pressed and character has already ID set.

**Headers:**
- Authorization: {access-key}
- Content-Type: "text/json"


#### Response 200
All is correctly set, character was saved.

```
{
	"status": "success",
	"toast": "Character {name} was successfully saved."
}
```

#### Response 400
Something is incorrectly formed in the form.

```
{
	"status": "error",
	"toast": "Saved data is malformed."
}
```

#### Response 403
When the user tries to save the changes to the character that does not belong to them.

```
{
	"status": "error",
	"toast": "You have no rights to alter this character."
}
```

#### Response 404
When the character ID does not exist, there is no record to update.

```
{
	"status": "warning",
	"toast": "Could not save the character. Character does not exist."
}
```

#### Response 500
When saving character fails serverside.

```
{
	"status": "error",
	"toast": "Could not save the character. Internal server error."
}
```

### Get the character `GET /char/{id}`
Returns data of the character.


**Headers:**
- Authorization: {access-key}
- Content-Type: "text/json"

#### Response 200

```
{
	"status": "success",
	"toast": "Character {name} was loaded.",
	"character": #character
}
```

#### Response 403
Returns when user tries to check different character than their own.

```
{
	"status": "error",
	"toast": "You have no rights to read this character."
}
```

#### Response 404
Returns when ID of a non-existing character is returned.

{
	"status": "warning",
	"toast": "Character does not exist."
}

### Removing the character `DELETE /user/{id}`
Tries to remove the character with given ID.

**Headers:**
- Authorization: {access-key}
- Content-Type: "text/json"

#### Response 200

```
{
	"status": "success",
	"toast": "The character {name} was successfully installed."
}
```

#### Response 403
Pops-out when user tries to remove the character that does not belong to him.

```
{
	"status": "warning",
	"toast": "You have no rights to read this character."
}
```

#### Response 404
When the character with given ID does not exist in the database.

```
{
	"status": "warning",
	"toast": "Character with given ID does not exist."
}
```


## Game Master section
This set of endpoints handles section for managing player groups.

### Get list of adventures `GET /adventures`
Obtain list of dungeons made by user

**Headers:**
- Authorization: {access-key}
- Content-Type: text/json

#### Response 200
Data is obtained.

```
{
	"status": "success",
	"toast": "Dungeons were successfully loaded.",
	"dungeons: [
		...
	]
}
```

#### Response 500
Returns when server fails to process the request.

```
{
	"status": "error",
	"toast": "Internal server error. Could not fetch adventures."
}
```

### Create new adventure `POST /adventure`
Attempts to create new adventure.

**Headers:**
- Authorization: {access-key}
- Content-Type: text/json

```
{
	"id": int|null,
	"title": string(24),
	"notes": text
}
```

#### Response 200
All is good and adventure was created.

```
{
	"status": "success",
	"toast": "Adventure {name} was successfully created."
}
```
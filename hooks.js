import {Elem} from '/flwjs/flw.min.js';
import {app} from '/app.js';
import {Bond, Experience, Character, Item, Momentum} from '/classes.js';

const characterHooks = {
    selectGenre(ev, me) {
        me.set('genre', parseInt(ev.target.value));
        me.set('profession', 0);


        me.set('classes.professionHidden', (me.props.genre) ? '' : 'hidden');
        me.set('professionList', me.props.getProfessionList(me.props.genre));
    },

    checkStates(ev, me) {
        let states = me.props.states;
        let idx = ev.target.dataset.name;
        let value = states[idx].value;
        
        let valueDelta = ['wounded', 'stressed', 'exhausted'].indexOf(idx) > -1 ? 50 : 100;

        value -= valueDelta;

        if (value < 0) {
            value = 100;
        }

        states[idx].value = value;

        let enableCrisis = states.wounded.value == 100
            && states.stressed.value == 100
            && states.exhausted.value == 100;
        
        states.crisis.value = enableCrisis ? states.crisis.value : 0;

        me.set('classes.stateHidden', enableCrisis ? '' : 'hidden');
        me.set('classes.crisis', states.crisis.value ? 'character--crisis' : '');
        me.set('classes.threaten', states.threat.value ? 'character--threaten' : '');
        me.set('states', states);
    },

    addItem(ev, me) {
        let inventory = me.props.inventory;
        if (inventory.length < me.props.INVENTORY_LIMIT) {
            inventory.push(new Elem(
                'inventoryItem',
                new Item('', false, 0, false),
                itemHooks
            ));
        }

        if (inventory.length >= me.props.INVENTORY_LIMIT) {
            me.set('show.addItem', false);
        }

        me.set('inventory', inventory);
    },

    addExperience(ev, me) {
        let experiences = me.props.experiences;
        if (experiences.length < me.props.EXPERIENCE_LIMIT) {
            experiences.push(new Elem('experienceItem', new Experience(''), experienceHooks));
        }

        if (experiences.length >= me.props.EXPERIENCE_LIMIT) {
            me.set('show.addExperience', false);
        }

        me.set('experiences', experiences);
    },

    addBond(ev, me) {
        let bonds = me.props.bonds;

        bonds.push(new Elem('bondItem', new Bond('', 1), bondHooks));

        me.set('bonds', bonds);
    },

    // TEXTAREA

    autosize(ev, me) {
        let attr = ev.target.name;
        me.set(attr, ev.target.value);
        ev.target.style.height = '5px';
        let scrollHeight = ev.target.scrollHeight + 2;

        me.set('textareaSize.' + attr, scrollHeight);
    },

    roll(ev, me) {
        me.set('classes.dieRolled', 'die--rolled');

        setTimeout(() => {
            me.set('die.previous', me.props.die.current);
            me.set('die.current', me.props.rollDie());
        }, 200);

        setTimeout(() => {
            me.set('classes.dieRolled', '');
        }, 500);
    },

    save(ev, me) {
        if (!me.props.name.length && !confirm('Unnamed character will not be saved. Return to menu?')) {
            return;
        }
        app.props.saveCharacter(me.props.store());
    },

    remove(ev, me) {
        if (confirm('Remove character `' + me.props.name + '`?')) {
            app.props.removeCharacter(me.props.id);
        }
    }
};

const itemHooks = {
    strikeStart(ev, me) {
        ev.stopPropagation();

        me.set('strikeTimeout', new Date());
    },

    strikeCancel(ev, me) {
        ev.stopPropagation();

        const now = new Date();
        const diff = now.getTime() - me.get('strikeTimeout').getTime();
        const scratches = me.props.scratches;

        if (diff < 600) {
            if (scratches < 6) {
                me.set('scratches', scratches + 1);
                me.set('scratchMarks', '/'.repeat(scratches + 1));        
                return;
            }

            me.set('classes.strike', 'item__scratches--strike');
            return;
        }

        if (scratches == 0) {
            return;
        }

        if (me.get('strike')) {
            me.set('strike', false);
            me.set('classes.strike', '');
            me.set('scratches', 0);
            me.set('scratchMarks', '/'.repeat(0));
            return;
        }

        me.set('strike', true);
        me.set('classes.strike', 'item__scratches--strike');
    },

    scratch(ev, me) {
        ev.stopPropagation();
        console.log('scratching');
        if (me.props.strikeTimeout !== null) {
            me.set('strikeTimeout', null);
            return;
        }

        if (me.props.strike) {
            return;
        }

        me.set('scratches', me.props.scratches < 6 ? me.props.scratches + 1 : 0);
        me.set('scratchMarks', '/'.repeat(me.props.scratches));
    },

    swipeStart(ev, me) {
        let char = app.get('character');
        char.props.swipers.inventory.defineSwipeArea();
        char.props.swipers.inventory.start(ev);
    },

    swipeMove(ev, me) {
        let char = app.get('character');
        let swp = char.props.swipers.inventory;

        swp.move(ev);
        if (swp && ['swiping'].includes(swp.status)) {
            me.set('classes.dragged', 'list__item--dragged');

            let limit = swp.direction.horizontal == 'left' ? 33 : 20;
            let xdist = swp.distance.horizontal < limit ? swp.distance.horizontal : limit;

            me.set('pos', swp.direction.horizontal == 'left' ? - xdist : xdist);
            return;
        }
    },

    swipeEnd(ev, me) {
        me.set('classes.dragged', '');
        let char = app.get('character');
        let swp = char.props.swipers.inventory;

        if (me.props.pos <= -33 && swp.direction.horizontal == 'left') {
            me.props.remove(me.id);
            swp.end();
            char.props.sortInventory();
            char.renderer.update('inventory');
            return;
        }

        if (me.props.pos >= 20 && swp.direction.horizontal == 'right') {
            me.props.equip(me);
            me.set('pos', 0);
            swp.end();
            char.props.sortInventory();
            char.renderer.update('inventory');
            return;
        }
        
        swp.end();
        me.set('pos', 0);
        ev.target.focus();
    },

    noContextMenu(ev, me) {
        ev.preventDefault();
        ev.stopPropagation();
        console.log('no context menu');
    }
};

const momentumHooks = {
    swipeStart(ev, me) {
        let char = app.get('character');
        char.props.swipers.momentum.start(ev);
    },

    swipeMove(ev, me) {
        let char = app.get('character');
        let swp = char.props.swipers.momentum;

        swp.move(ev);
        if (swp && (swp.direction.horizontal == 'left' || me.props.value < 7) && ['swiping'].includes(swp.status)) {
            me.set('classes.dragged', 'momentum__content--dragged');
            
            let limit = swp.direction.horizontal == 'left' ? 33 : 20;
            let xdist = swp.distance.horizontal < limit ? swp.distance.horizontal : limit;

            me.set('pos', swp.direction.horizontal == 'left' ? - xdist : xdist);
            return;
        }
    },

    swipeEnd(ev, me) {
        me.set('classes.dragged', '');
        let char = app.get('character');
        let swp = char.props.swipers.momentum;

        if (me.props.pos <= -33 && swp.direction.horizontal == 'left') {
            me.set('pos', 0);
            me.set('value', 0);
            me.props.updateSquares(0);
            me.renderer.update('squares');
            swp.end();
            return;
        }
        
        if (me.props.value < 7 && me.props.pos >= 20 && swp.direction.horizontal == 'right') {
            let value = me.props.value + 1;
            me.set('pos', 0);
            me.set('value', value);
            me.props.updateSquares(value);
            me.renderer.update('squares');
            swp.end();
            return;
        }
        
        swp.end();
        me.set('pos', 0);
    }
};

const experienceHooks = {
    swipeStart(ev, me) {
        me.set('classes.dragged', 'list__item--dragged');
        let char = app.get('character');
        char.props.swipers.experiences.defineSwipeArea();
        char.props.swipers.experiences.start(ev);
    },

    swipeMove(ev, me) {
        let char = app.get('character');
        let swp = char.props.swipers.experiences;

        if (swp && ['prepared', 'swiping'].includes(swp.status)) {
            swp.move(ev);

            let limit = 33;
            let xdist = swp.distance.horizontal < limit ? swp.distance.horizontal : limit;

            me.set('pos', swp.direction.horizontal == 'left' ? - xdist : 0);
            return;
        }
    },

    swipeEnd(ev, me) {
        me.set('classes.dragged', '');
        let char = app.get('character');
        let swp = char.props.swipers.experiences;

        if (me.props.pos <= -33 && swp.direction.horizontal == 'left') {
            me.props.remove(me.id);
            swp.end();
            return;
        }

        swp.end();
        me.set('pos', 0);
        ev.target.focus();
    }
};

const bondHooks = {
    swipeStart(ev, me) {
        me.set('classes.dragged', 'list__item--dragged');
        let char = app.get('character');
        char.props.swipers.bonds.defineSwipeArea();
        char.props.swipers.bonds.start(ev);
    },

    swipeMove(ev, me) {
        let char = app.get('character');
        let swp = char.props.swipers.bonds;

        if (swp && ['prepared', 'swiping'].includes(swp.status)) {
            swp.move(ev);

            let limit = 33;
            let xdist = swp.distance.horizontal < limit ? swp.distance.horizontal : limit;

            me.set('pos', swp.direction.horizontal == 'left' ? - xdist : 0);
        }
    },

    swipeEnd(ev, me) {
        me.set('classes.dragged', '');
        let char = app.get('character');
        let swp = char.props.swipers.bonds;

        if (me.props.pos <= -33 && swp.direction.horizontal == 'left') {
            me.props.remove(me.id);
            swp.end();
            return;
        }

        me.set('pos', 0);
        swp.end();

        ev.target.focus();
    }
};

const menuCharacterHooks = {
    selectCharacter(ev, me) {
        let chars = app.get('characters');

        for (let i in (chars)) {
            if (chars[i].id == me.props.id) {
                let inv = chars[i].inventory.reduce((x, y) => {
                    let item = new Elem(
                        'inventoryItem',
                        new Item(y.name, y.equipped, y.scratches, y.strike),
                        itemHooks
                    );
                    x.push(item);
                    return x;
                }, []);

                let xp = chars[i].experiences.reduce((x, y) => {
                    x.push(new Elem('experienceItem', new Experience(y.experience), experienceHooks));
                    return x;
                }, []);

                let bnds = chars[i].bonds.reduce((x, y) => {
                    x.push(new Elem('bondItem', new Bond(y.creature, y.bond), bondHooks));
                    return x;
                }, []);

                let momentum = new Elem(
                    'momentum',
                    new Momentum(chars[i].momentum),
                    momentumHooks
                );


                let char = new Character(
                    chars[i].id,
                    chars[i].name,
                    chars[i].profession,
                    chars[i].motivation,
                    inv,
                    momentum,
                    chars[i].notes,
                    xp,
                    bnds,
                    chars[i].die,
                    chars[i].states,
                    chars[i].classes,
                    chars[i].textareaSize,
                    chars[i].genre || 0,
                );

                app.set('character', new Elem('character', char, characterHooks));
                app.set('logoSmaller', 'logo--smaller');
                app.get('menu').set('hidden', 'hidden');
                app.props.initSwipers(app.get('character'));

                return;
            }
        }

        console.error('Cannot find character with ID ' + me.props.id);
    }
};

const menuHooks = {
    addCharacter(ev, me) {
        let now = new Date();
        let char = new Character(now.getTime());
        char.classes.genreHidden = '';

        app.set('character', new Elem('character', char, characterHooks));
        app.set('logoSmaller', 'logo--smaller');
        me.set('hidden', 'hidden');
        app.props.initSwipers(app.get('character'));
    },

    reset (ev, me) {
        if (confirm('Reset all data?')) {
            app.set('characters', []);
            app.props.save();

            me.props.generateCharList();
        }
    },

    checkCodes (ev, me) {
        alert('Checking codes', ev.target.value);
        fetch('http://localhost:8000/code/' + ev.target.value, {
            type: 'GET',
            headers: [],
        });
    }
};

const appHooks = {
    switchTheme(ev, me) {
        let theme = me.props.theme;

        theme = theme.search(/dark/) > -1 ? 'app--light' : 'app--dark';
        me.set('theme', theme);

        me.props.save();
    }
};

export {
    characterHooks,
    itemHooks,
    momentumHooks,
    experienceHooks,
    bondHooks,
    menuCharacterHooks,
    menuHooks,
    appHooks
};
import {Elem} from '/flwjs/flw.min.js';
import {Swiper} from '/swiper/swiper.min.js';
import {app} from '/app.js';
import {menuHooks, menuCharacterHooks, momentumHooks} from '/hooks.js';

class MenuCharacter {
    constructor(id, name) {
        this.id = id;
        this.name = name;
    }
}

class Menu {
    constructor() {
        this.hidden = '';
        this.charList = [];
        this.code = '';
    }

    generateCharList() {
        let charList = [];
        let chars = app.get('characters') || [];
        for (let i in chars) {
            charList.push(new Elem(
                'menuCharacter',
                new MenuCharacter(chars[i].id, chars[i].name),
                menuCharacterHooks
            ));
        }

        this.charList = charList;

        let menuElem = app.get('menu');

        if (menuElem) {
            menuElem.renderer.update();
        }
    }
}

class Character {
    constructor(
        id,
        name,
        profession,
        motivation,
        inventory,
        momentum,
        notes,
        experiences,
        bonds,
        die,
        states,
        classes,
        textareaSize,
        genre
    ) {
        this.id = id;
        this.name = name || '';
        this.profession = profession || 0;
        this.motivation = motivation || {group: '', personal: ''};
        this.inventory = inventory || [];
        this.momentum = momentum || new Elem('momentum', new Momentum(0), momentumHooks);
        this.notes = notes || '';
        this.experiences = experiences || [];
        this.bonds = bonds || [];
        this.die = die || {
            current: this.rollDie(),
            previous: this.rollDie(),
        };
        this.states = states || {
            wounded: {
                title: 'Wounded',
                value: 0
            },
            stressed: {
                title: 'Stressed',
                value: 0
            },
            exhausted: {
                title: 'Exhausted',
                value: 0
            },
            threat: {
                title: 'Threat',
                value: 0
            },
            crisis: {
                title: 'Crisis',
                value: 0
            }
        };

        this.classes = classes || {
            genreHidden: 'hidden',
            professionHidden: 'hidden',
            stateHidden: 'hidden',
            crisis: '',
            threaten: '',
            dieRolled: ''
        };

        this.INVENTORY_LIMIT = 15;
        this.EXPERIENCE_LIMIT = 10;
        this.EQUIP_LIMIT = 5;

        this.show = {
            addItem: this.inventory.length < this.INVENTORY_LIMIT,
            addExperience: this.experiences.length < this.EXPERIENCE_LIMIT,
        };

        this.swipers = {
            inventory: null,
            experiences: null,
            bonds: null,
            momentum: null,
        };

        this.textareaSize = textareaSize || {
            motivation: {group: 37, personal: 37},
            notes: 37
        };

        this.GENRE_PROFESSIONS = [
            {genre: "Fantasy",          professions: ["knight", "scoundrel", "monk", "vagrant", "merchant", "scout"]},
            {genre: "Cthulhu",          professions: ["investigator", "occultist", "survivor", "antiquarian", "field researcher", "doctor"]},
            {genre: "Goblins",          professions: ["tinker", "bruiser", "shaman", "rogue", "trader", "scout"]},
            {genre: "Star Wars",        professions: ["jedi knight", "smuggler", "force monk", "spacer", "trader", "rebel scout"]},
            {genre: "Japan",            professions: ["samurai", "ninja", "monk", "ronin", "merchant", "scout"]},
            {genre: "Alien",            professions: ["star pilot", "engineer", "synth", "drifter", "trader", "recon specialist"]},
            {genre: "Three Musketeers", professions: ["musketeer", "rogue", "priest", "vagabond", "trader", "scout"]},
            {genre: "Bubblegum",        professions: ["pop star", "rebel", "zen guru", "nomad", "fashionista", "urban explorer"]},
            {genre: "Cyberpunk",        professions: ["street samurai", "netrunner", "techno-monk", "drifter", "fixer", "infiltrator"]},
            {genre: "Stone-Age",        professions: ["warrior", "tracker", "shaman", "nomad", "trader", "gatherer"]}
        ];
        
        this.genre = genre || 0;
        this.genreList = this.getGenreList();
        this.professionList = this.getProfessionList(this.genre);

        if (!this.genre) {
            this.classes.genreHidden = '';
        }
    }

    getGenreList() {
        return this.GENRE_PROFESSIONS.reduce((x, y, z) => {
            x.push(new Elem('selectOption', {value: z + 1, name: y.genre}));
            return x;
        }, [new Elem('selectOption', {value: 0, name: '- choose genre -'})]);
    }

    getProfessionList(genre) {
        if (!genre) {
            return [];
        }

        return this.GENRE_PROFESSIONS[genre -1].professions.reduce((x, y, z) => {
            x.push(new Elem('selectOption', {value: z + 1, name: y}));
            return x;
        }, [new Elem('selectOption', {value: 0, name: '- choose profession -'})]);
    }

    

    // INVENTORY, EXPERIENCES, BONDS
    sortInventory() {
        this.inventory = this.inventory.sort((a, b) => {
            const aVal = a.props.equipped ? 0 : 1;
            const bVal = b.props.equipped ? 0 : 1;

            if (aVal != bVal) {
                return aVal <= bVal ? -1 : 1;
            }

            return 0;
        });
    }

    

    store() {
        this.classes.genreHidden = 'hidden';

        let inv = this.inventory.reduce((x, y) => {
            x.push({
                name: y.props.name,
                equipped: y.props.equipped,
                scratches: y.props.scratches,
                strike: y.props.strike
            });
            return x;
        }, []);

        let xp = this.experiences.reduce((x, y) => {
            x.push({
                experience: y.props.experience
            });
            return x;
        }, []);

        let bnds = this.bonds.reduce((x, y) => {
            x.push({
                creature: y.props.creature,
                bond: y.props.bond
            });
            return x;
        }, []);

        return {
            id: this.id,
            name: this.name,
            profession: this.profession,
            motivation: this.motivation,
            inventory: inv,
            momentum: this.momentum.props.value,
            notes: this.notes,
            experiences: xp,
            bonds: bnds,
            die: this.die,
            states: this.states,
            classes: this.classes,
            textareaSize: this.textareaSize,
            genre: this.genre
        };
    }

    rollDie() {
        return Math.floor(Math.random() * 6) + 1;
    }
}

class Item {
    constructor(
        name,
        equipped,
        scratches,
        strike
    ) {
        this.name = name || '';
        this.equipped = equipped || false;
        this.scratches = scratches || 0;
        this.strike = strike || false;

        this.pos = 0;
        this.strikeTimeout = null;
        this.scratchMarks = '/'.repeat(scratches);
        this.classes = {
            dragged: '',
            equipped: equipped ? 'list__item--equipped' : '',
            strike: strike ? 'item__scratches--strike' : '',
        };
    }

    equip(me) {
        if (me.props.equipped) {
            me.set('classes.equipped', '');
            me.set('equipped', false);
            return;
        }

        let char = app.get('character');
        let equipped = char.props.inventory.reduce((x, y) => {
            return (y.props.equipped) ? x + 1 : x;
        }, 0);

        if (equipped < char.props.EQUIP_LIMIT) {
            me.set('classes.equipped', 'list__item--equipped');
            me.set('equipped', true);

        }
        else {
            me.set('classes.equipped', '');
        }
    }

    remove(id) {
        let char = app.get('character');

        for (let i = 0; i < char.props.inventory.length; i++) {
            if (char.props.inventory[i].id == id) {
                char.props.inventory.splice(i,1);
                break;
            }
        }

        char.props.show.addItem = true;
        char.renderer.update('inventory');
    }
}

class Momentum {
    constructor(
        value
    ) {
        this.value = value || 0;
        
        this.pos = 0;
        this.classes = {
            dragged: ''
        };

        this.squares = [];
        this.updateSquares(this.value);

    }

    updateSquares(value) {
        this.squares = [];
        for (let i = 0; i < value; i++) {
            this.squares.push(new Elem('<span class="momentum__square"></span>'));
        }
    }
}

class Experience {
    constructor(
        experience
    ) {
        this.experience = experience || '';
        this.classes = {
            dragged: ''
        };
        this.pos = 0;
    }

    remove(id) {
        let char = app.get('character');

        for (let i = 0; i < char.props.experiences.length; i++) {
            if (char.props.experiences[i].id == id) {
                char.props.experiences.splice(i,1);
                break;
            }
        }

        char.props.show.addExperience = true;
        char.renderer.update('experiences');
    }
}

class Bond {
    constructor(
        creature,
        bond
    ) {
        this.creature = creature || '';
        this.bond = bond || 1;
        this.classes = {
            dragged: ''
        };
        this.pos = 0;
    }

    remove(id) {
        let char = app.get('character');

        for (let i = 0; i < char.props.bonds.length; i++) {
            if (char.props.bonds[i].id == id) {
                char.props.bonds.splice(i, 1);
                break;
            }
        }

        char.props.show.addItem = true;
        char.renderer.update('bonds');
    }
}

class App {
    constructor() {
        this.logoSmaller = '';
        this.theme = 'app--light';
        this.characters = [];
        this.menu = new Elem('menu', new Menu(), menuHooks);
        this.character = null;
    }

    saveCharacter(charData) {
        let saved = false;

        if (charData.name.length) {
            for (let i in this.characters) {
                if (this.characters[i].id == charData.id) {
                    this.characters[i] = charData;
                    saved = true;
                    break;
                }
            }

            if (!saved) {
                this.characters.push(charData);
            }
        }

        this.menu.props.generateCharList();
        this.toMenu();
    }

    removeCharacter(charId) {
        for (let i in this.characters) {
            if (this.characters[i].id == charId) {
                this.characters.splice(i, 1);
                break;
            }
        }

        this.menu.props.generateCharList();
        this.toMenu();
    }

    toMenu() {
        app.get('menu').set('hidden', '');
        app.set('logoSmaller', '');
        app.del('character');
        app.props.save();
    }

    load(data) {
        this.theme = data.theme;

        for (let i = 0; i < data.characters.length; i++) {
            this.characters = data.characters;
        }

        this.menu.props.generateCharList();

        app.renderer.update();
    }

    save() {
        let charData = this.characters.reduce((x, y) => {
            x.push(y);
            return x;
        }, []);

        let data = {
            theme: this.theme,
            characters: charData
        }

        window.localStorage.dngn = JSON.stringify(data);
    }

    initSwipers(char) {
        char.set('swipers.inventory', new Swiper(document.querySelector('.inventory')));
        char.set('swipers.experiences', new Swiper(document.querySelector('.experiences')));
        char.set('swipers.bonds', new Swiper(document.querySelector('.bonds')));
        char.set('swipers.momentum', new Swiper(document.querySelector('.momentum')));
    }
}

export {
    MenuCharacter,
    Menu,
    Character,
    Item,
    Momentum,
    Experience,
    Bond,
    App
};
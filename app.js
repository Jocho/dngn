import {Flw, Elem} from '/flwjs/flw.min.js';
import {App} from '/classes.js';
import {appHooks} from '/hooks.js';

let main = new Flw('.main');
let app = new Elem('app', new App(), appHooks);

main.addElem(app);

main.run();

document.querySelector('.inventory')

if (typeof window.localStorage.dngn !== 'undefined') {
    app.props.load(JSON.parse(window.localStorage.dngn));
}

if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js');
}

export {app};